import React, { Component } from 'react';
import '../style/components.css'
import AppService from '../service/app_service'
import Search from './search'

class Form extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            job: '',
            open: false
        };
    }

    

handleButtonClick = () => {
    this.setState(state => {
        return {
            open: !state.open
        };
    });
};

handleClickOutside = event => {
    if (this.container.current && !this.container.current.contains(event.target)) {
        this.setState({
            open: false
        });
    }
};
   
    render() {
        if (this.state.open) {
            return (
                <div className="form-box">
                    <form onSubmit={this.create}>
                        <div className="form-group">
                            <input type="text" className="form-control" id="inputName" placeholder="Nome" ref={(input) => this.name = input} />
                        </div>
                        <div className="form-group">
                            <input type="email" className="form-control" id="inputEmail" placeholder="Email" ref={(input) => this.email = input} />
                        </div>
                        <div className="form-group">
                            <input type="job" className="form-control" id="inputJob" placeholder="Em qual área" ref={(input) => this.job = input} />
                        </div>
                        <button type="submit" className="btn btn-primary" value="Submit">Submit</button>
                    </form>
                </div>
            );
        }
        else {
            return null
        }
       
    }

    create = async (event)=> {
        try {
            event.preventDefault()
            const appService = new AppService()
            const res = await appService.create(this.name.value, this.email.value, this.job.value)
            console.log(res)
        }
        catch (err) {
            console.log(err)
        }

    }
}

export default Form;