const express = require('express')
const UserService = require('../../api/common/users/service')

class UserRouterService {
    constructor(server, routeName, dbConn) {
        this.routeName = routeName
        this.server = server
        this.router = express.Router()
        this.dbConn = dbConn

    }

    async create(req, res) {

        console.log(req)
         let response = {
             error: false,
             message: "Usuário criado com sucesso!"
         }

         if (!req.body) {
             res.status(400)
             response.message = "Favor enviar os parâmetros (Name, email and job)!"
             response.error = true
         }
         else if (!req.body.name) {
             res.status(400)

             response.message = "Favor informar o nome do usuário!"
             response.error = true
         }
         else if (!req.body.email) {
             res.status(400)
             response.message = "Favor informar o email do usuário!"
             response.error = true
         }
         else if (!req.body.job) {
             res.status(400)
             response.message = "Favor informar o cargo do usuário!"
             response.error = true
         }
         else {
             const userService = new UserService(this.dbConn)
             const params = userService.validateParamsToCreateUser(req.body.name, req.body.email, req.body.job)
             
             if (params.isValid) {
                 try {
                     await userService.createUser(params)
                 }
                 catch (err) {
                     console.log(`Opss!! Something went wrong --> ${err}`)
                     res.status(500)
                     response.message = "Oppss!! Ocorreu um erro ao criar um novo usuário"
                     response.error = true
                 }
             }
             else {
                 res.status(400)
                 response.message = params.message
                 response.error = true
             }
         }

        //  return response
        return res.send(response)
    }

    
    update(req, res) {
        
    }

    async find(req, res) {
        let response = {
            data: [],
            error: false,
            message: "Usuários econtrados!"
        }
        const userService = new UserService(this.dbConn)
        try {
            const founded = await userService.findUser(req.body)

            if (founded.length > 0) {
                response.data = founded
            }
            else {
                response.message = 'Não existe usuários cadastrados'
            }
        }
        catch (err) {
            res.status(500)
            response.message = 'Não foi possivel encontrar usuários'
        }

        return res.send(response)
    }


    build() {
        this.router.post('/create', this.create.bind(this))
        this.router.post('/update', this.update.bind(this))
        this.router.post('/find', this.find.bind(this))
        this.server.use(this.routeName, this.router)
    }
}

module.exports = UserRouterService