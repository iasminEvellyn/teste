import React, { Component } from 'react';
import '../style/components.css'

class Header extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-6">
                    <p className="title-header">Setup</p>
                </div>
                <div className="col-6">
                    <p className="name-user-header">Olá, Carlos</p>
                </div>
            </div>

        );
    }
}

export default Header;