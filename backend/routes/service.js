const helmet = require('helmet');
const compression = require('compression')
const bodyParser = require('body-parser')
const UserRouterService = require('./user/service')

class RouterService {
    constructor(server, dbConn) {
        this.server = server
        this.dbConn = dbConn
    }

    middleware() {
        this.server.use(helmet())
        this.server.use(compression())
        this.server.use(bodyParser.json({ limit: '10mb', extended: true }))
        this.server.use(bodyParser.urlencoded({ extended: true }))
        this.server.all('*', function (req, res, next) {
            var responseSettings = {
                "AccessControlAllowOrigin": req.headers.origin,
                "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
                "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
                "AccessControlAllowCredentials": true
            }
            res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials)
            res.header("Access-Control-Allow-Origin", "*")
            res.header("Access-Control-Allow-Headers", "Origin, Content-Type, X-Auth-Token")
            res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
            res.header()
            next()
        })
    }

    run() {
        new UserRouterService(this.server, '/users', this.dbConn).build()
    }

    async listen() {
        this.middleware()
        this.run()

        this.server.listen(process.env.PORT, function () {
            console.log(`Listen at http://localhost:${process.env.PORT}`)
        })
    }
}

module.exports = RouterService