import React, { Component } from 'react';
import '../style/components.css';
import AppService from '../service/app_service'
import babyGroot from '../public/img/baby-groot.jpg'

class Users extends Component {
    constructor() {
        super()
        this.state = {
            users: []
        }

        this.findUsers = this.findUsers.bind(this)
        }

    componentWillMount() {
        this.findUsers()
    }

    async findUsers() {
        const appService = new AppService()
        const res = await appService.findAll()
        console.log(res)
        this.setState({users : res.data})
    }

    render() {
        if (this.state.users) {
            return (
                    this.state.users.map((value, index) => {
                        return (
                            <div key={index} className="elements-users">
                                <img src={babyGroot} className="image-user" alt="Baby Groot" />
                                <div className="box-text-user">
                                    <p className="p-user-name">{value.name}</p>
                                    <p >{value.email}</p>
                                </div>
                            <div className="icons-user"></div>
                            </div>
                        )
                    })
            );
        }
       
    }
}

export default Users;