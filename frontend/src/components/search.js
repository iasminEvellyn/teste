import React, { Component } from 'react';
import '../style/components.css'
import Form from './form'

class Search extends Component {

    render() {
        return (
            <div className="row search-row">
                <div className="input-group input-search">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">@</span>
                    </div>
                    <input type="text" className="form-control" placeholder="Digite uma palvra chave e aperte enter para buscar"
                        aria-label="Username" aria-describedby="basic-addon1" />
                </div>

                <div className="btn-group btn-group-toggle buttons-crud" data-toggle="buttons">
                    <span className="title-button-radio"> Visualizar:</span>
                    <label className="btn btn-secondary radio-view">
                        <input className="radio-view" type="radio" name="options" /> FREELAS
                </label>
                    <label className="btn btn-secondary radio-view">
                        <input type="radio" name="options" /> USUÁRIOS
                </label>
                </div>
                <button type="button" className="btn btn-success btn-new-user" onClick={this.props.handleButtonClick}> NOVO MEMBROs</button>
            </div>
        );
    }
}

export default Search;
