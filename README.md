Desafio_Rock

#Backend
>Para executar o backend siga os seguintes passos -->

* Acesse a pasta backend em um terminal.

* Execute o comando npm i, ele irá instalar todas as dependências que se encontram no package.json

* Copie o env.local e crie o .env

* Altere os IPS's para que consigam testar localmente

* Para rodar o backend basta executar o comando **node server**


#Frontend
> Para executar o frontend siga os seguintes passos -->

* Acesse a pasta frontend em um terminal

* Execute o comando npm i, ele irá instalar todas as dependências que se encontram no package.json

* Altere as informações do .env com o IP da máquina que estará rodando o seu backend

* Agora é so executar! Para isso digite **npm start**
