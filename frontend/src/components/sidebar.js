import React, { Component } from 'react';
import '../style/components.css'

class SideBar extends Component {
  render() {
    return (
      <div className="sidebar light-grey bar-block">
        <div className='circle-menu'></div>
        <div className="item">

          {/* TASKS MENU */}

          <div className="itens-general item-task"></div>
          <div className="item-title">TASKS</div>

          {/* CONTENTS MENU */}

          <div className="itens-general item-contents"></div>
          <div className="item-title">CONTENTS</div>

          {/* SETUP MENU */}
          <div className="itens-general item-setup"></div>
          <div className="item-title">SETUP</div>
        </div>
      </div>

    );
  }
}

export default SideBar;
