const _ = require('lodash')
const UserModel = require('./model')
class UserService {
    constructor(dbConn) {
        this.model = new UserModel(dbConn)
    }


    validateParamsToCreateUser(name, email, job){
        let response = {
            isValid: null,
            message: null,
            params: {
                name: name,
                email: email,
                job :job
            }
        }
        if (!this.isValidName(name).isValid) {
            response.isValid = false
            response.message = "o nome do usuário não é valido"
        }
        else if (!this.isValidName(email).isValid) {
            response.isValid = false
            response.message = "o email do usuário não é valido"
        }
        else {
            response.isValid = true
        }

        return response
    }

    async createUser(params) {
        try {
            var user = {
                name: params.params.name,
                email: params.params.email,
                job: params.params.job,
            }
            const response = await this.model.create(user)
            return Promise.resolve(response)
            
        }
        catch (err) {
            return Promise.reject(err)
        }
       
    }

    async findUser(params) {
        try {
            const response = await this.model.find(params)
            return Promise.resolve(response)
        }
        catch (err) {
            console.log('Erro ao encontrar usuários')
           return Promise.reject(err) 
        }
    }


    // Validate Params

    isValidName(name) {
        let response = {
            isValid: true,
            message: "",
            name: _.trim(name)
        }

        let occurencesAllowed = {
            E: true,
            A: true,
            AS: true,
            DA: true,
            DAS: true,
            DE: true,
            DES: true,
            DO: true,
            DOS: true
        }

        if (_.isString(name)) {
            const explodedName = _.words(_.trim(name))
            if (explodedName.length >= 2) {
                for (let i = 0; i < explodedName.length; i++) {
                    if ((explodedName[i].length <= 2 || explodedName[i].indexOf(".") !== -1) && !(occurencesAllowed[_.toUpper(explodedName[i])])) {
                        response.isValid = false
                        response.message = "O nome não deve ser abreviado"
                    } else if ((occurencesAllowed[_.toUpper(explodedName[i])]) && (i === (explodedName.length - 1) || i === 0)) {
                        response.isValid = false
                        response.message = "Digite o nome completo"
                    }
                }
            } else {
                response.isValid = false
                response.message = "O nome deve conter nome e sobrenome"
            }
        } else {
            response.isValid = false
            response.message = "O nome deve conter ao menos uma letra"
        }
        return response
    }

    isValidEmail(email) {
        const explodedByArroba = email.split("@")
        const response = {
            isValid: true,
            email: email
        }

        const emailReplaced = email.replace(/[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/gi, "")

        if (emailReplaced.length > 0) {
            response.isValid = false
        } else if (explodedByArroba.length = 2) {
            if (explodedByArroba[0].length > 3) {
                const explodedByDot = explodedByArroba[1].split(".")

                if (explodedByDot.length < 2) {
                    for (let i = 0; i < explodedByDot.length; i++) {
                        if (explodedByDot[i].length <= 2) {
                            response.isValid = false
                        }
                    }
                }

                if (explodedByArroba[0].startsWith(".") || explodedByArroba[0].endsWith(".")) {
                    response.isValid = false
                }
            } else {
                response.isValid = false
            }
        } else {
            response.isValid = false
        }

        return response
    }
  
}

module.exports = UserService