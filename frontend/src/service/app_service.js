import Service from './../service/service';

export default class AppService {

 async create(name = null, email = null, job = null) {
    this.Service = new Service()
    const request = {
      name: name,
      email: email,
      job:job
    }
    return await this.Service.post(request, 'users/create')
  }

  async update(id = null, name = null, email = null, job = null) {
    this.Service = new Service()
    const request = {
      name: name,
      email: email,
      job: job
    }
    return await this.Service.post(request, 'users/update')
  }

  async findAll() {
    this.Service = new Service()
    const request = {}
    return await this.Service.post(request, 'users/find')
  }
}

