import React, { Component } from 'react';
import '../style/components.css'

class Tabs extends Component {
    render() {
        return (
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <a className="nav-link active" href="#">MINHA EQUIPE</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#">TAB ITEM</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#">TAB ITEM</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#">TAB ITEM</a>
                </li>
            </ul>
        );
    }
}

export default Tabs;