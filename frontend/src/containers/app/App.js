import React, { Component } from 'react';
import '../../style/app.css';
import '../../style/components.css'


//Import Components
import Sidebar from '../../components/sidebar'
import Tabs from '../../components/tabs'
import Header from '../../components/header'
import Search from '../../components/search'
import Users from '../../components/users'
import Form from '../../components/form'


class App extends Component {
  render() {
    return (
      <div>
        <Form />
        <div className="row">
          <div className="col-1" >
            <Sidebar />
          </div>
          <div className="col-12 header">
            <Header />
            <Tabs />
          </div>
        </div>
        <Search />
        <div className="row box-users">
          <Users />
        </div>
      </div>
    );
  }
}

export default App;
