
export default class Service {
  
  async post(param, path) {
    console.log(path)
    console.log(param)
    try {
      const request = {
        method: 'POST',
        body: JSON.stringify(param),
        headers: new Headers({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      }
      const res = await fetch(process.env.REACT_APP_API_REST + path, request)
      return res.json()
    }
    catch (err) {
      console.log(err)
      return err
    }
  }
}