const moment = require('moment')
require("moment/min/locales.min")

moment.updateLocale('pt-BR')

const _userModelSchema = {
  name: {
    type: String
  },
  email: {
    type: String
  },
  job: {
    type: String
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  }
}

class UserModel {
  constructor(db) {
    this.db = db
    this.schema = new db.Schema(_userModelSchema, {
      timestamps: true
    })
    try {
      this.model = db.model('users')
    } catch (err) {
      this.model = db.model('users', this.schema)
    }
  }

  create(props) {
    if (!props) {
      props = {}
    }
    return this.model.create(props)
  }


 async find(params) {
   const res = await this.model.find({})
   return Promise.resolve(res)
 }

  update(id, props) {
    return this.model.updateOne({ _id: id }, props, { updateTimestamps: true })
  }
}

module.exports = UserModel