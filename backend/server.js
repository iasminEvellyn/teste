const Dotenv = require('dotenv').config({ path: './env/.env' })
const RouterService = require('./routes/service')
const express = require('express')
const server = express()


//Conection mongo
const db = require('./database/service')
const dbConnection = process.env.DB_MONGO


async function start() {
  try {
    const dbConn = await db.connect(dbConnection, { useNewUrlParser: true })
     new RouterService(server, dbConn).listen()
    // router.listen()
  }
  catch (err) {
    console.log(err)
  }
 
}

start()

